package com.github.shtratos.orderbook;

import java.util.Objects;
import java.util.StringJoiner;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Represents a limit order
 */
public final class LimitOrder implements Order {

    private final String id;
    private final OrderSide side;
    private final int quantity;
    private final short price;
    private final long timestamp;

    public static LimitOrder from(String id, OrderSide side, int quantity, short price, long timestamp) {
        return new LimitOrder(id, side, quantity, price, timestamp);
    }

    private LimitOrder(String id, OrderSide side, int quantity, short price, long timestamp) {
        Objects.requireNonNull(id, "id is null!");
        Objects.requireNonNull(side, "side is null!");
        checkArgument(price > 0, "price must be positive, was: %s", price);
        checkArgument(quantity > 0, "quantity must be positive, was: %s", quantity);
        this.id = id;
        this.side = side;
        this.quantity = quantity;
        this.price = price;
        this.timestamp = timestamp;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public OrderSide getSide() {
        return side;
    }

    @Override
    public int getQuantity() {
        return quantity;
    }

    @Override
    public int getDisclosedQuantity() {
        return quantity;
    }

    @Override
    public short getPrice() {
        return price;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public LimitOrder fillPartially(int quantityFilled, LogicalClock clock) {
        checkArgument(quantity > quantityFilled,
                "can not fill more than %s, requested: %s", quantity, quantityFilled);
        int newQuantity = quantity - quantityFilled;
        return new LimitOrder(id, side, newQuantity, price, timestamp);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LimitOrder that = (LimitOrder) o;
        return id.equals(that.id) &&
                quantity == that.quantity &&
                price == that.price &&
                timestamp == that.timestamp &&
                side == that.side;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, side, quantity, price, timestamp);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", LimitOrder.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("direction=" + side)
                .add("amount=" + quantity)
                .add("price=" + price)
                .add("timestamp=" + timestamp)
                .toString();
    }
}
