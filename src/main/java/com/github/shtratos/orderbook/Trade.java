package com.github.shtratos.orderbook;

import java.util.Objects;
import java.util.StringJoiner;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Represents a trade action.
 */
public final class Trade {

    private final String buyOrderId;
    private final String sellOrderId;
    private final short price;
    private final int quantity;

    public Trade(String buyOrderId, String sellOrderId, short price, int quantity) {
        Objects.requireNonNull(buyOrderId, "buyOrderId is null!");
        Objects.requireNonNull(sellOrderId, "sellOrderId is null!");
        checkArgument(!buyOrderId.isEmpty(), "buyOrderId must be non-empty string!");
        checkArgument(!sellOrderId.isEmpty(), "sellOrderId must be non-empty string!");
        checkArgument(!Objects.equals(buyOrderId, sellOrderId),
                "buyOrderId must not be the same as sellOrderId: %s, %s", buyOrderId, sellOrderId);
        checkArgument(price > 0, "price must be positive, was: %s", price);
        checkArgument(quantity > 0, "quantity must be positive, was: %s", quantity);
        this.buyOrderId = buyOrderId;
        this.sellOrderId = sellOrderId;
        this.price = price;
        this.quantity = quantity;
    }

    public String getBuyOrderId() {
        return buyOrderId;
    }

    public String getSellOrderId() {
        return sellOrderId;
    }

    public short getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public Trade changeQuantity(int newQuantity) {
        checkArgument(newQuantity > 0, "new trade quantity must be positive, was: %s", newQuantity);
        return new Trade(buyOrderId, sellOrderId, price, newQuantity);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trade trade = (Trade) o;
        return price == trade.price &&
                quantity == trade.quantity &&
                Objects.equals(buyOrderId, trade.buyOrderId) &&
                Objects.equals(sellOrderId, trade.sellOrderId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(buyOrderId, sellOrderId, price, quantity);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Trade.class.getSimpleName() + "[", "]")
                .add("buyOrderId=" + buyOrderId)
                .add("sellOrderId=" + sellOrderId)
                .add("price=" + price)
                .add("quantity=" + quantity)
                .toString();
    }
}
