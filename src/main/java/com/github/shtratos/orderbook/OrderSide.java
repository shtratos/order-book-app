package com.github.shtratos.orderbook;

import com.google.common.base.Preconditions;

/**
 * Marks on which side of the book the order is.
 */
public enum OrderSide {
    BUY {
        @Override
        public OrderSide otherSide() {
            return SELL;
        }
    },
    SELL {
        @Override
        public OrderSide otherSide() {
            return BUY;
        }
    };

    public static OrderSide from(String s) {
        Preconditions.checkArgument("B".equals(s) || "S".equals(s), "Unknown order side: %s", s);
        return "B".equals(s) ? BUY : SELL;
    }

    public abstract OrderSide otherSide();

}
