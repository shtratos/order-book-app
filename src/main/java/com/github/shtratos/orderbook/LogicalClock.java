package com.github.shtratos.orderbook;

/**
 * A logical clock that provides monotonously increasing sequence of ticks.
 * <p>
 * We assume that long integer overflow does not happen while the application runs.
 */
public interface LogicalClock {

    /**
     * Get current logical time in unspecified units
     *
     * @return current timestamp
     */
    long getTicks();
}
