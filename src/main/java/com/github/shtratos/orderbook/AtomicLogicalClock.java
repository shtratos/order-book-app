package com.github.shtratos.orderbook;

import java.util.concurrent.atomic.AtomicLong;

public class AtomicLogicalClock implements LogicalClock {

    private final AtomicLong ticker = new AtomicLong(1);

    @Override
    public long getTicks() {
        return ticker.getAndIncrement();
    }
}
