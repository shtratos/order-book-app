package com.github.shtratos.orderbook;

import java.util.Objects;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Represents an Iceberg order
 */
public final class IcebergOrder implements Order {

    private final String id;
    private final OrderSide side;
    private final int totalQuantity;
    private final int peakSize;
    private final int maxPeakSize;
    private final short price;
    private final long timestamp;

    public static IcebergOrder from(String id, OrderSide side,
                                    int totalQuantity, int maxPeakSize,
                                    short price, long timestamp) {
        checkArgument(maxPeakSize <= totalQuantity,
                "maxPeakSize %s can not be more than totalQuantity %s", maxPeakSize, totalQuantity);
        return new IcebergOrder(id, side, totalQuantity, maxPeakSize, maxPeakSize, price, timestamp);
    }

    public static IcebergOrder from(String id, OrderSide side,
                                    int totalQuantity, int peakSize, int maxPeakSize,
                                    short price, long timestamp) {
        checkArgument(maxPeakSize <= totalQuantity,
                "maxPeakSize %s can not be more than totalQuantity %s", maxPeakSize, totalQuantity);
        checkArgument(peakSize <= maxPeakSize,
                "peakSize %s can not be more than maxPeakSize %s", peakSize, maxPeakSize);
        return new IcebergOrder(id, side, totalQuantity, peakSize, maxPeakSize, price, timestamp);
    }

    public IcebergOrder(String id, OrderSide side,
                        int totalQuantity, int peakSize, int maxPeakSize,
                        short price, long timestamp) {
        Objects.requireNonNull(id, "id is null!");
        Objects.requireNonNull(side, "side is null!");
        checkArgument(price > 0, "price must be positive, was: %s", price);
        checkArgument(totalQuantity > 0, "totalQuantity must be positive, was: %s", totalQuantity);
        this.id = id;
        this.side = side;
        this.totalQuantity = totalQuantity;
        this.peakSize = peakSize;
        this.maxPeakSize = maxPeakSize;
        this.price = price;
        this.timestamp = timestamp;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public OrderSide getSide() {
        return side;
    }

    @Override
    public int getQuantity() {
        return totalQuantity;
    }

    @Override
    public int getDisclosedQuantity() {
        return peakSize;
    }

    public int getMaxPeakSize() {
        return maxPeakSize;
    }

    @Override
    public short getPrice() {
        return price;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public IcebergOrder fillPartially(int quantityFilled, LogicalClock clock) {
        checkArgument(getQuantity() > quantityFilled,
                "can not fill more than %s, requested: %s", getQuantity(), quantityFilled);

        final long newTimestamp = (quantityFilled >= peakSize) ? clock.getTicks() : getTimestamp();
        final int newPeakSize;
        if (quantityFilled >= peakSize) {
            newPeakSize = getMaxPeakSize();
        } else {
            newPeakSize = peakSize - quantityFilled;
        }
        final int newTotalQuantity = getQuantity() - quantityFilled;

        return new IcebergOrder(getId(), getSide(),
                newTotalQuantity, newPeakSize, getMaxPeakSize(),
                getPrice(), newTimestamp);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IcebergOrder that = (IcebergOrder) o;
        return totalQuantity == that.totalQuantity &&
                peakSize == that.peakSize &&
                maxPeakSize == that.maxPeakSize &&
                price == that.price &&
                timestamp == that.timestamp &&
                id.equals(that.id) &&
                side == that.side;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, side, totalQuantity, peakSize, maxPeakSize, price, timestamp);
    }
}
