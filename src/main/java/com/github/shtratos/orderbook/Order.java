package com.github.shtratos.orderbook;

public interface Order {

    /**
     * The unique identifier of the order.
     */
    String getId();

    /**
     * Whether the order is to buy or sell.
     */
    OrderSide getSide();

    /**
     * The quantity being bought or sold.
     * This should be a whole number that is greater than zero.
     */
    int getQuantity();

    /**
     * The maximum quantity, that may be displayed.
     * This should be a whole number. For Iceberg Orders, this
     * will be greater than zero but less than the order quantity.
     * For Limit Orders, this will be the same as Order Quantity
     */
    int getDisclosedQuantity();

    /**
     * The maximum/minimum price a buy/sell order may be
     * executed at. This value should be greater than zero and
     * a multiple of the instrument’s ‘Tick’.
     */
    short getPrice();

    /**
     * Logical timestamp when the order has been entered.
     */
    long getTimestamp();

    /**
     * Does an offered price satisfy order constraints?
     *
     * @param offeredPrice potential trade price
     * @return if the price offered is acceptable
     */
    default boolean willAcceptPrice(short offeredPrice) {
        switch (getSide()) {
            case BUY:
                return offeredPrice <= getPrice();
            case SELL:
                return offeredPrice >= getPrice();
            default:
                throw new IllegalStateException("Unknown order side: " + getSide());
        }
    }

    /**
     * Adjust an order to reflect that it has been partially filled.
     *
     * @param quantityFilled number of units that has been filled
     * @param clock          logical clock that can supply fresh timestamps
     * @return new adjusted order instance
     */
    Order fillPartially(int quantityFilled, LogicalClock clock);

}
