package com.github.shtratos.orderbook.ui;

import com.github.shtratos.orderbook.Order;
import com.github.shtratos.orderbook.OrderBook;
import com.google.common.collect.ImmutableList;
import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.asciitable.CWC_FixedWidth;
import de.vandermeer.asciithemes.TA_Grid;
import de.vandermeer.asciithemes.TA_GridConfig;
import de.vandermeer.asciithemes.a7.A7_Grids;
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.function.BiConsumer;

public final class OrderBookFormatter {

    public static final int TOTAL_COLUMNS = 6;
    public static final int ID_COLUMN_WIDTH = 10;
    public static final int QTY_COLUMN_WIDTH = 13;
    public static final int PRICE_COLUMN_WIDTH = 7;

    private final NumberFormat numberFormat = NumberFormat.getIntegerInstance(Locale.UK);

    public OrderBookFormatter() {
    }

    public String format(OrderBook book) {
        Objects.requireNonNull(book, "book is null!");
        final AsciiTable at = new AsciiTable();
        configureTableStyle(at);
        addHeaders(at);
        addBody(book, at);
        return at.render();
    }

    private void configureTableStyle(AsciiTable at) {
        final TA_Grid gridConfig = A7_Grids.minusBarPlus();
        gridConfig.addTopruleCharacterMap(TA_GridConfig.RULESET_NORMAL,
                ' ', '-', '+', '+', '-');
        gridConfig.addBottomRuleCharacterMap(TA_GridConfig.RULESET_NORMAL,
                ' ', '-', '+', '+', '-');
        // the rule below is required to correctly handle subsequent header rows without rule between them
        gridConfig.getCharacterMap()
                .put(TA_GridConfig.RULESET_NORMAL | TA_GridConfig.CHAR_CONTENT_RULE | TA_GridConfig.TYPE_DOWN, '|');
        at.getContext().setGrid(gridConfig);
        at.getRenderer().setCWC(new CWC_FixedWidth()
                .add(ID_COLUMN_WIDTH).add(QTY_COLUMN_WIDTH).add(PRICE_COLUMN_WIDTH)
                .add(PRICE_COLUMN_WIDTH).add(QTY_COLUMN_WIDTH).add(ID_COLUMN_WIDTH));
    }

    private void addHeaders(AsciiTable at) {
        at.addRule();
        at.addRow(null, null, "BUY", null, null, "SELL")
                .setPaddingLeft(1);
        at.addRow("Id", "Volume", "Price", "Price", "Volume", "Id")
                .setPaddingLeft(1);
        at.addRule();
    }

    private void addBody(OrderBook book, AsciiTable at) {
        final List<List<Object>> rows = new ArrayList<>();
        zipOrders(book, (buy, sell) -> {
            final List<Object> row = new ArrayList<>(TOTAL_COLUMNS);
            if (buy != null) {
                row.add(buy.getId());
                row.add(numberFormat.format(buy.getDisclosedQuantity()));
                row.add(numberFormat.format(buy.getPrice()));
            } else {
                row.addAll(ImmutableList.of("", "", ""));
            }
            if (sell != null) {
                row.add(numberFormat.format(sell.getPrice()));
                row.add(numberFormat.format(sell.getDisclosedQuantity()));
                row.add(sell.getId());
            } else {
                row.addAll(ImmutableList.of("", "", ""));
            }
            rows.add(row);
        });

        if (rows.isEmpty()) {
            at.addRow("", "", "", "", "", "");
        } else {
            for (List<Object> rowValues : rows) {
                at.addRow(rowValues)
                        .setTextAlignment(TextAlignment.RIGHT);
            }
        }
        at.addRule();
    }

    private void zipOrders(OrderBook book, BiConsumer<Order, Order> consumer) {
        final ImmutableList<Order> buys = book.getBuyOrders();
        final ImmutableList<Order> sells = book.getSellOrders();
        final int maxLength = Math.max(buys.size(), sells.size());
        for (int i = 0; i < maxLength; i++) {
            Order buy = (i < buys.size()) ? buys.get(i) : null;
            Order sell = (i < sells.size()) ? sells.get(i) : null;
            consumer.accept(buy, sell);
        }
    }

}
