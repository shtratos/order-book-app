package com.github.shtratos.orderbook.ui;

import com.github.shtratos.orderbook.IcebergOrder;
import com.github.shtratos.orderbook.LimitOrder;
import com.github.shtratos.orderbook.Order;
import com.github.shtratos.orderbook.OrderFactory;
import com.github.shtratos.orderbook.OrderSide;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;

import java.util.List;
import java.util.Objects;

public final class Parser {
    private final OrderFactory orderFactory;

    public Parser(OrderFactory orderFactory) {
        Objects.requireNonNull(orderFactory, "orderFactory is null!");
        this.orderFactory = orderFactory;
    }

    public Order parseOrder(String line) {
        Objects.requireNonNull(line, "line is null!");
        final List<String> parts = Splitter.on(",").limit(5).trimResults().splitToList(line);
        Preconditions.checkArgument(parts.size() == 4 || parts.size() == 5,
                "Wrong number of parts [%s] in `%s`", parts.size(), line);
        try {
            if (parts.size() == 4) {
                return makeLimitOrder(parts);
            } else {
                return makeIcebergOrder(parts);
            }
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(String.format(
                    "Could not parse line `%s`: %s", line, e.getMessage()), e);
        }
    }

    private LimitOrder makeLimitOrder(List<String> parts) {
        final OrderSide orderSide = OrderSide.from(parts.get(0));
        final String id = parts.get(1);
        final short price = Short.parseShort(parts.get(2));
        final int quantity = Integer.parseInt(parts.get(3));

        return this.orderFactory.makeLimitOrder(id, orderSide, quantity, price);
    }

    private IcebergOrder makeIcebergOrder(List<String> parts) {
        final OrderSide orderSide = OrderSide.from(parts.get(0));
        final String id = parts.get(1);
        final short price = Short.parseShort(parts.get(2));
        final int quantity = Integer.parseInt(parts.get(3));
        final int peakSize = Integer.parseInt(parts.get(4));

        return this.orderFactory.makeIcebergOrder(id, orderSide, quantity, peakSize, price);
    }
}
