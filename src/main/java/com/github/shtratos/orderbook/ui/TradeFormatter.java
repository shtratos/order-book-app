package com.github.shtratos.orderbook.ui;

import com.github.shtratos.orderbook.Trade;

import java.util.Objects;

public final class TradeFormatter {
    public TradeFormatter() {
    }

    public String format(Trade trade) {
        Objects.requireNonNull(trade, "trade is null!");
        return String.format("%s,%s,%d,%d",
                trade.getBuyOrderId(), trade.getSellOrderId(), trade.getPrice(), trade.getQuantity());
    }
}
