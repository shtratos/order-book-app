package com.github.shtratos.orderbook;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.LinkedHashMultimap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.List;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.function.Function;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

/**
 * Represents an order book
 * <p>
 * It contains all currently active orders and allows matching them.
 */
public class OrderBook {

    private final OrderFactory orderFactory;

    private final PriorityQueue<Order> buys;
    private final PriorityQueue<Order> sells;
    private final EnumMap<OrderSide, PriorityQueue<Order>> sides;

    public OrderBook(OrderFactory orderFactory) {
        this.orderFactory = Objects.requireNonNull(orderFactory, "orderFactory is null!");
        final Comparator<Order> buyOrdering = Comparator
                .comparing(Order::getPrice, Comparator.reverseOrder())
                .thenComparingLong(Order::getTimestamp);
        final Comparator<Order> sellOrdering = Comparator
                .comparingInt(Order::getPrice)
                .thenComparingLong(Order::getTimestamp);
        this.buys = new PriorityQueue<>(buyOrdering);
        this.sells = new PriorityQueue<>(sellOrdering);
        this.sides = new EnumMap<>(ImmutableMap.of(
                OrderSide.BUY, this.buys,
                OrderSide.SELL, this.sells));
    }

    /**
     * Apply an order to the book, possibly generating trades.
     *
     * @param order an order to be applied to the book
     */
    public ImmutableList<Trade> applyOrder(Order order) {
        Objects.requireNonNull(order, "order is null!");
        addOrder(order);
        return ImmutableList.copyOf(matchOrders());
    }

    private void addOrder(Order order) {
        sides.get(order.getSide()).add(order);
    }

    private int maxQuantityCanFill(Order restingOrder, int requestedQuantity) {
        checkArgument(requestedQuantity > 0,
                "quantity requested to fill must be positive! was: %s", requestedQuantity);
        if (restingOrder.getClass().equals(LimitOrder.class)) {
            return Math.min(restingOrder.getQuantity(), requestedQuantity);
        } else {
            return Math.min(restingOrder.getDisclosedQuantity(), requestedQuantity);
        }
    }

    private List<Trade> matchOrders() {
        if (!buys.isEmpty() && !sells.isEmpty()) {
            final Order bestBuy = buys.peek();
            final Order bestSell = sells.peek();
            if (bestBuy.getPrice() >= bestSell.getPrice()) {
                final Order aggressingOrder = (bestBuy.getTimestamp() > bestSell.getTimestamp()) ? bestBuy : bestSell;
                final PriorityQueue<Order> passiveSide = getOtherSideOf(aggressingOrder);

                int quantityLeftToFill = aggressingOrder.getQuantity();
                int quantityFilled = 0;
                final List<Trade> trades = new ArrayList<>();

                while (!passiveSide.isEmpty()
                        && aggressingOrder.willAcceptPrice(passiveSide.peek().getPrice())
                        && quantityLeftToFill > 0
                ) {
                    final Order restingOrder = Objects.requireNonNull(passiveSide.poll(), "queue can not be empty!");
                    final int maxQuantityCanFill = maxQuantityCanFill(restingOrder, quantityLeftToFill);
                    if (quantityLeftToFill >= maxQuantityCanFill) {
                        quantityLeftToFill -= maxQuantityCanFill;
                    } else {
                        quantityLeftToFill = 0;
                    }

                    final Trade trade = makeTrade(aggressingOrder, restingOrder, maxQuantityCanFill);
                    trades.add(trade);
                    quantityFilled += maxQuantityCanFill;

                    if (restingOrder.getQuantity() > trade.getQuantity()) {
                        addOrder(orderFactory.fillPartially(restingOrder, trade.getQuantity()));
                    }
                }


                final Order filledOrder = sides.get(aggressingOrder.getSide()).poll();
                // not a bug! we want to make sure we remove the SAME order instance that has just been filled
                checkState(filledOrder == aggressingOrder,
                        "order at the head of the queue has changed unexpectedly: expected %s, found %s",
                        aggressingOrder, filledOrder);
                if (quantityLeftToFill > 0) {
                    // put unfilled remainder of the aggressing order back on the queue
                    addOrder(orderFactory.fillPartially(aggressingOrder, quantityFilled));
                }
                return compactTrades(trades);
            }
        }
        return ImmutableList.of();
    }

    private List<Trade> compactTrades(List<Trade> trades) {
        final LinkedHashMultimap<String, Trade> groups = LinkedHashMultimap.create();
        Function<Trade, String> key = t ->
                String.join("|", t.getBuyOrderId(), t.getSellOrderId(), String.valueOf(t.getPrice()));
        for (Trade trade : trades) {
            groups.put(key.apply(trade), trade);
        }
        final List<Trade> mergedTrades = new ArrayList<>();
        for (Collection<Trade> tradeGroup : groups.asMap().values()) {
            checkState(!tradeGroup.isEmpty(), "group of trades can not be empty. Groups: %s", groups);
            if (tradeGroup.size() == 1) {
                mergedTrades.addAll(tradeGroup);
            } else {
                final Trade firstTrade = tradeGroup.iterator().next();
                final int totalTradeQuantity = tradeGroup.stream().mapToInt(Trade::getQuantity).sum();
                mergedTrades.add(firstTrade.changeQuantity(totalTradeQuantity));
            }
        }
        return ImmutableList.copyOf(mergedTrades);
    }

    private Trade makeTrade(Order aggressing, Order resting, int tradeQuantity) {
        checkArgument(aggressing.getSide().otherSide() == resting.getSide(),
                "Trade orders %s and %s must be on different sides!", aggressing, resting);
        checkArgument(aggressing.willAcceptPrice(resting.getPrice()),
                "%s will not accept price of %s!", aggressing, resting);
        checkArgument(tradeQuantity > 0,
                "trade quantity must be positive! was: %s Orders: [%s, %s]", tradeQuantity, aggressing, resting);
        final String buyId = aggressing.getSide() == OrderSide.BUY ? aggressing.getId() : resting.getId();
        final String sellId = aggressing.getSide() == OrderSide.SELL ? aggressing.getId() : resting.getId();
        return new Trade(buyId, sellId, resting.getPrice(), tradeQuantity);
    }

    private PriorityQueue<Order> getOtherSideOf(Order order) {
        return sides.get(order.getSide().otherSide());
    }

    /**
     * Get total quantity on the buy side of the book, including hidden iceberg orders
     */
    public int getTotalBuyQuantity() {
        return buys.stream().mapToInt(Order::getQuantity).sum();
    }

    /**
     * Get total quantity on the sell side of the book, including hidden iceberg orders
     */
    public int getTotalSellQuantity() {
        return sells.stream().mapToInt(Order::getQuantity).sum();
    }

    /**
     * Get total visible quantity on the buy side of the book
     */
    public int getTotalDisclosedBuyQuantity() {
        return buys.stream().mapToInt(Order::getDisclosedQuantity).sum();
    }

    /**
     * Get total visible quantity on the sell side of the book
     */
    public int getTotalDisclosedSellQuantity() {
        return sells.stream().mapToInt(Order::getDisclosedQuantity).sum();
    }

    /**
     * List orders on the buy side in the matching order
     */
    public ImmutableList<Order> getBuyOrders() {
        return ImmutableList.sortedCopyOf(buys.comparator(), buys);
    }

    /**
     * List orders on the sell side in the matching order
     */
    public ImmutableList<Order> getSellOrders() {
        return ImmutableList.sortedCopyOf(sells.comparator(), sells);
    }
}
