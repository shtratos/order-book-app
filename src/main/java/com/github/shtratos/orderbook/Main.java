package com.github.shtratos.orderbook;

import com.github.shtratos.orderbook.ui.OrderBookFormatter;
import com.github.shtratos.orderbook.ui.Parser;
import com.github.shtratos.orderbook.ui.TradeFormatter;
import com.google.common.collect.ImmutableList;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.stream.Stream;

/**
 * Entry point into the application.
 */
public class Main {

    public static void main(String[] args) {
        try {
            final BufferedReader reader = new BufferedReader(
                    new InputStreamReader(System.in, StandardCharsets.UTF_8));
            runOrderExecutor(reader, System.out);
        } catch (Exception e) {
            e.printStackTrace(System.err);
            System.exit(1);
        }
    }

    /**
     * Runs the execution loop:
     * <ul>
     *  <li>reads limit and iceberg orders</li>
     *  <li>applies them to an order book</li>
     *  <li>prints trades and order book state snapshots</li>
     *  </ul>
     *
     * @param textInput  where to read orders from
     * @param textOutput where to print trades and order book state snapshots
     */
    static void runOrderExecutor(BufferedReader textInput, PrintStream textOutput) {
        final LogicalClock clock = new AtomicLogicalClock();
        final OrderFactory orderFactory = new OrderFactory(clock);
        final Parser orderParser = new Parser(orderFactory);
        final OrderBook orderBook = new OrderBook(orderFactory);

        final OrderBookFormatter orderBookFormatter = new OrderBookFormatter();
        final TradeFormatter tradeFormatter = new TradeFormatter();

        readOrders(textInput, orderParser).forEach(newOrder -> {
            final ImmutableList<Trade> trades = orderBook.applyOrder(newOrder);
            for (Trade trade : trades) {
                textOutput.println(tradeFormatter.format(trade));
            }
            textOutput.println(orderBookFormatter.format(orderBook));
        });
    }

    private static Stream<Order> readOrders(BufferedReader textInput, Parser orderParser) {
        return textInput.lines()
                .map(String::trim)
                .filter(line -> !line.isEmpty() && !line.startsWith("#"))
                .map(orderParser::parseOrder);
    }
}
