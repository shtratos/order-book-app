package com.github.shtratos.orderbook;

import java.util.Objects;

/**
 * Ensures that orders are created in the right order.
 */
public class OrderFactory {

    private final LogicalClock clock;

    public OrderFactory(LogicalClock clock) {
        this.clock = Objects.requireNonNull(clock, "clock is null!");
    }

    public LimitOrder makeLimitOrder(String orderId, OrderSide side, int quantity, short price) {
        return LimitOrder.from(orderId, side, quantity, price, nextTimestamp());
    }

    public IcebergOrder makeIcebergOrder(String orderId, OrderSide side, int quantity, int peakSize, short price) {
        return IcebergOrder.from(orderId, side, quantity, peakSize, price, nextTimestamp());
    }

    public Order fillPartially(Order order, int quantityFilled) {
        return order.fillPartially(quantityFilled, clock);
    }

    private long nextTimestamp() {
        return clock.getTicks();
    }

}
