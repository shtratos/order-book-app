package com.github.shtratos.orderbook;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import static com.google.common.truth.Truth.assertThat;

public class MainTest {

    private BufferedReader wrapInput(String... lines) {
        return new BufferedReader(new StringReader(String.join("\n", lines)));
    }

    private String run(String... inputLines) throws UnsupportedEncodingException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (PrintStream textOutput = new PrintStream(baos, true, "UTF-8")) {
            Main.runOrderExecutor(wrapInput(inputLines), textOutput);
        }
        return new String(baos.toByteArray(), StandardCharsets.UTF_8);
    }

    @Test
    public void reads_orders_matches_them_in_order_book_and_prints_trades_and_order_book_state() throws Exception {
        assertThat(run(
                "B,100322,5103,7500",
                "S,100345,5103,100000,10000"
        )).isEqualTo("" +
                "+-----------------------------------------------------------------+\n" +
                "| BUY                            | SELL                           |\n" +
                "| Id       | Volume      | Price | Price | Volume      | Id       |\n" +
                "+----------+-------------+-------+-------+-------------+----------+\n" +
                "|    100322|        7,500|  5,103|       |             |          |\n" +
                "+-----------------------------------------------------------------+\n" +
                "100322,100345,5103,7500\n" +
                "+-----------------------------------------------------------------+\n" +
                "| BUY                            | SELL                           |\n" +
                "| Id       | Volume      | Price | Price | Volume      | Id       |\n" +
                "+----------+-------------+-------+-------+-------------+----------+\n" +
                "|          |             |       |  5,103|        2,500|    100345|\n" +
                "+-----------------------------------------------------------------+\n");
    }

    @Test
    public void produces_nothing_on_empty_input() throws Exception {
        assertThat(run("", "")).isEqualTo("");
    }
}
