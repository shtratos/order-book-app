package com.github.shtratos.orderbook;

import com.google.common.collect.ImmutableList;

import java.util.concurrent.atomic.AtomicLong;

import static com.google.common.truth.Truth.assertThat;

/**
 * Various helpers for writing concise tests
 */
public abstract class BaseTest {

    private final AtomicLong idGenerator = new AtomicLong(1000);

    protected final LogicalClock clock = new AtomicLogicalClock();
    protected final OrderFactory orderFactory = new OrderFactory(clock);

    protected String nextOrderId() {
        return String.valueOf(idGenerator.incrementAndGet());
    }

    protected LimitOrder buy(int quantity, int price) {
        return buy(nextOrderId(), quantity, price);
    }

    protected LimitOrder buy(String id, int quantity, int price) {
        return orderFactory.makeLimitOrder(id, OrderSide.BUY, quantity, (short) price);
    }

    protected LimitOrder sell(int quantity, int price) {
        return sell(nextOrderId(), quantity, price);
    }

    protected LimitOrder sell(String id, int quantity, int price) {
        return orderFactory.makeLimitOrder(id, OrderSide.SELL, quantity, (short) price);
    }

    @SuppressWarnings("SameParameterValue")
    protected IcebergOrder buyIceberg(int quantity, int peak, int price) {
        return orderFactory.makeIcebergOrder(nextOrderId(), OrderSide.BUY, quantity, peak, (short) price);
    }

    protected IcebergOrder sellIceberg(int quantity, int peak, int price) {
        return orderFactory.makeIcebergOrder(nextOrderId(), OrderSide.SELL, quantity, peak, (short) price);
    }

    protected Trade trade(String buyOrderId, String sellOrderId, int price, int quantity) {
        return new Trade(buyOrderId, sellOrderId, (short) price, quantity);
    }

    protected void addPassiveOrder(OrderBook book, Order order) {
        final ImmutableList<Trade> trades = book.applyOrder(order);
        assertThat(trades).isEmpty();
    }


}
