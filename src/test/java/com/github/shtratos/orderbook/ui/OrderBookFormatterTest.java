package com.github.shtratos.orderbook.ui;

import com.github.shtratos.orderbook.BaseTest;
import com.github.shtratos.orderbook.OrderBook;
import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

public class OrderBookFormatterTest extends BaseTest {

    private OrderBookFormatter makeFormatter() {
        return new OrderBookFormatter();
    }

    private OrderBook makeBook() {
        return new OrderBook(orderFactory);
    }

    @Test
    public void can_format_empty_table() {
        // given
        final OrderBook book = makeBook();
        // when / then
        assertThat(makeFormatter().format(book)).isEqualTo("" +
                "+-----------------------------------------------------------------+\n" +
                "| BUY                            | SELL                           |\n" +
                "| Id       | Volume      | Price | Price | Volume      | Id       |\n" +
                "+----------+-------------+-------+-------+-------------+----------+\n" +
                "|          |             |       |       |             |          |\n" +
                "+-----------------------------------------------------------------+");
    }

    @Test
    public void can_format_lone_buy_order() {
        // given
        final OrderBook book = makeBook();
        addPassiveOrder(book, buy("1234567890", 1_234_567_890, 32_503));
        // when / then
        assertThat(makeFormatter().format(book)).isEqualTo("" +
                "+-----------------------------------------------------------------+\n" +
                "| BUY                            | SELL                           |\n" +
                "| Id       | Volume      | Price | Price | Volume      | Id       |\n" +
                "+----------+-------------+-------+-------+-------------+----------+\n" +
                "|1234567890|1,234,567,890| 32,503|       |             |          |\n" +
                "+-----------------------------------------------------------------+");
    }

    @Test
    public void can_format_lone_sell_order() {
        // given
        final OrderBook book = makeBook();
        addPassiveOrder(book, sell("1234567890", 1_234_567_890, 32_503));
        // when / then
        assertThat(makeFormatter().format(book)).isEqualTo("" +
                "+-----------------------------------------------------------------+\n" +
                "| BUY                            | SELL                           |\n" +
                "| Id       | Volume      | Price | Price | Volume      | Id       |\n" +
                "+----------+-------------+-------+-------+-------------+----------+\n" +
                "|          |             |       | 32,503|1,234,567,890|1234567890|\n" +
                "+-----------------------------------------------------------------+");
    }

    @Test
    public void can_format_several_orders_with_different_number_of_orders_on_each_side() {
        // given
        final OrderBook book = makeBook();
        addPassiveOrder(book, buy("1234567890", 1_234_567_890, 32_503));
        addPassiveOrder(book, buy("1138", 7_500, 31_502));
        addPassiveOrder(book, sell("1234567891", 1_234_567_890, 32_504));
        addPassiveOrder(book, sell("6808", 7_777, 32_505));
        addPassiveOrder(book, sell("42100", 3_000, 32_507));
        // when / then
        assertThat(makeFormatter().format(book)).isEqualTo("" +
                "+-----------------------------------------------------------------+\n" +
                "| BUY                            | SELL                           |\n" +
                "| Id       | Volume      | Price | Price | Volume      | Id       |\n" +
                "+----------+-------------+-------+-------+-------------+----------+\n" +
                "|1234567890|1,234,567,890| 32,503| 32,504|1,234,567,890|1234567891|\n" +
                "|      1138|        7,500| 31,502| 32,505|        7,777|      6808|\n" +
                "|          |             |       | 32,507|        3,000|     42100|\n" +
                "+-----------------------------------------------------------------+");
    }

    @Test
    public void shows_only_disclosed_quantity_for_iceberg_orders() {
        // given
        final OrderBook book = makeBook();
        addPassiveOrder(book, buyIceberg(1_234_567, 10000, 100));
        addPassiveOrder(book, sellIceberg(9_234_567, 20000, 110));
        // when / then
        assertThat(makeFormatter().format(book)).isEqualTo("" +
                "+-----------------------------------------------------------------+\n" +
                "| BUY                            | SELL                           |\n" +
                "| Id       | Volume      | Price | Price | Volume      | Id       |\n" +
                "+----------+-------------+-------+-------+-------------+----------+\n" +
                "|      1001|       10,000|    100|    110|       20,000|      1002|\n" +
                "+-----------------------------------------------------------------+");
    }

}
