package com.github.shtratos.orderbook;

import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.util.List;

import static com.google.common.truth.Truth.assertThat;

public class OrderBookTest extends BaseTest {

    protected OrderBook makeBook() {
        return new OrderBook(orderFactory);
    }

    @Test
    public void buy_orders_are_sorted_by_decreasing_price_and_decreasing_timestamp() {
        // given
        OrderBook book = makeBook();
        // when
        final LimitOrder order1 = buy(30, 98);
        final LimitOrder order2 = buy(10, 99);
        final LimitOrder order3 = buy(40, 98);
        final LimitOrder order4 = buy(20, 99);
        addPassiveOrder(book, order1);
        addPassiveOrder(book, order2);
        addPassiveOrder(book, order3);
        addPassiveOrder(book, order4);
        // then
        assertThat(book.getTotalBuyQuantity()).isEqualTo(100);
        assertThat(book.getTotalSellQuantity()).isEqualTo(0);
        // orders are sorted by price desc and timestamp desc
        assertThat(book.getBuyOrders())
                .containsExactly(order2, order4, order1, order3).inOrder();
    }

    @Test
    public void sell_orders_are_sorted_by_increasing_price_and_decreasing_timestamp() {
        // given
        OrderBook book = makeBook();
        // when
        final LimitOrder order1 = sell(30, 98);
        final LimitOrder order2 = sell(10, 99);
        final LimitOrder order3 = sell(40, 98);
        final LimitOrder order4 = sell(20, 99);
        addPassiveOrder(book, order1);
        addPassiveOrder(book, order2);
        addPassiveOrder(book, order3);
        addPassiveOrder(book, order4);
        // then
        assertThat(book.getTotalBuyQuantity()).isEqualTo(0);
        assertThat(book.getTotalSellQuantity()).isEqualTo(100);
        // orders are sorted by price asc and timestamp desc
        assertThat(book.getSellOrders())
                .containsExactly(order1, order3, order2, order4).inOrder();
    }

    @Test
    public void when_there_is_no_price_overlap_no_trades_are_made() {
        // given
        OrderBook book = makeBook();
        // when / then
        addPassiveOrder(book, sell(40, 101));
        addPassiveOrder(book, buy(40, 99));
        addPassiveOrder(book, sell(50, 102));
        addPassiveOrder(book, buy(60, 98));
        // all orders stay on the book
        assertThat(book.getTotalBuyQuantity()).isEqualTo(100);
        assertThat(book.getTotalSellQuantity()).isEqualTo(90);
    }

    @Test
    public void buy_and_sell_orders_are_matched_on_price() {
        // given
        OrderBook book = makeBook();
        addPassiveOrder(book, buy(10, 101));
        // when
        final List<Trade> trades = book.applyOrder(sell(10, 101));
        // then
        assertThat(trades).containsExactly(trade("1001", "1002", 101, 10));
        // orders are removed
        assertThat(book.getTotalBuyQuantity()).isEqualTo(0);
        assertThat(book.getTotalSellQuantity()).isEqualTo(0);
    }

    @Test
    public void when_orders_are_matched_other_orders_do_not_change() {
        // given
        OrderBook book = makeBook();
        final LimitOrder order1 = buy(20, 98);
        final LimitOrder order2 = sell(30, 105);
        final LimitOrder order3 = buy(40, 99);
        final LimitOrder order4 = sell(50, 104);
        addPassiveOrder(book, order1);
        addPassiveOrder(book, order2);
        addPassiveOrder(book, order3);
        addPassiveOrder(book, order4);
        addPassiveOrder(book, buy(10, 101));
        // when
        final List<Trade> trades = book.applyOrder(sell(10, 101));
        // then
        assertThat(trades).containsExactly(trade("1005", "1006", 101, 10));
        // orders are removed
        assertThat(book.getTotalBuyQuantity()).isEqualTo(60);
        assertThat(book.getTotalSellQuantity()).isEqualTo(80);
        assertThat(book.getBuyOrders()).containsExactly(order3, order1).inOrder();
        assertThat(book.getSellOrders()).containsExactly(order4, order2).inOrder();
    }

    /**
     * From LSE MIT20 - Guide to the Trading System:
     * <quote>
     * All valid orders entered in the regular trading session that are available for immediate execution
     * attempt to aggress the book and will execute as far as permissible at the resting order’s limit
     * price.
     * </quote>
     */
    @Test
    public void when_sell_limit_order_aggresses_book_a_trade_is_made_at_resting_buy_order_price() {
        // given
        OrderBook book = makeBook();
        addPassiveOrder(book, buy(10, 105));
        // when
        final List<Trade> trades = book.applyOrder(sell(10, 100));
        // then
        assertThat(trades).containsExactly(trade("1001", "1002", 105, 10));
        // orders are removed
        assertThat(book.getTotalBuyQuantity()).isEqualTo(0);
        assertThat(book.getTotalSellQuantity()).isEqualTo(0);
    }

    /**
     * From LSE MIT20 - Guide to the Trading System:
     * <quote>
     * All valid orders entered in the regular trading session that are available for immediate execution
     * attempt to aggress the book and will execute as far as permissible at the resting order’s limit
     * price.
     * </quote>
     */
    @Test
    public void when_buy_limit_order_aggresses_book_a_trade_is_made_at_resting_sell_order_price() {
        // given
        OrderBook book = makeBook();
        addPassiveOrder(book, sell(10, 100));
        // when
        final List<Trade> trades = book.applyOrder(buy(10, 105));
        // then
        assertThat(trades).containsExactly(trade("1002", "1001", 100, 10));
        // orders are removed
        assertThat(book.getTotalBuyQuantity()).isEqualTo(0);
        assertThat(book.getTotalSellQuantity()).isEqualTo(0);
    }

    @Test
    public void when_limit_orders_match_only_partially_remainder_is_left() {
        // given
        OrderBook book = makeBook();
        addPassiveOrder(book, buy(20, 105));
        // when
        final List<Trade> trades = book.applyOrder(sell(15, 105));
        // then
        assertThat(trades).containsExactly(trade("1001", "1002", 105, 15));
        // unfilled remainder is left on the book
        assertThat(book.getTotalBuyQuantity()).isEqualTo(5);
        assertThat(book.getTotalSellQuantity()).isEqualTo(0);
        assertThat(book.getBuyOrders()).containsExactly(
                LimitOrder.from("1001", OrderSide.BUY, 5, (short) 105, 1));
    }

    @Test
    public void sell_limit_order_can_match_multiple_buy_limit_orders_at_the_same_price_in_time_priority() {
        // given
        OrderBook book = makeBook();
        addPassiveOrder(book, buy(10, 105));
        addPassiveOrder(book, buy(20, 105));
        addPassiveOrder(book, buy(30, 105));
        // when
        final List<Trade> trades = book.applyOrder(sell(40, 105));
        // then
        assertThat(trades).containsExactly(
                trade("1001", "1004", 105, 10),
                trade("1002", "1004", 105, 20),
                trade("1003", "1004", 105, 10)
        ).inOrder();
        // unfilled remainder of latest order is left on the book
        assertThat(book.getTotalBuyQuantity()).isEqualTo(20);
        assertThat(book.getTotalSellQuantity()).isEqualTo(0);
        assertThat(book.getBuyOrders()).containsExactly(
                LimitOrder.from("1003", OrderSide.BUY, 20, (short) 105, 3));
    }

    @Test
    public void buy_limit_order_can_match_multiple_sell_limit_orders_at_the_same_price_in_time_priority() {
        // given
        OrderBook book = makeBook();
        addPassiveOrder(book, sell(10, 105));
        addPassiveOrder(book, sell(20, 105));
        addPassiveOrder(book, sell(30, 105));
        // when
        final List<Trade> trades = book.applyOrder(buy(40, 105));
        // then
        assertThat(trades).containsExactly(
                trade("1004", "1001", 105, 10),
                trade("1004", "1002", 105, 20),
                trade("1004", "1003", 105, 10)
        ).inOrder();
        // unfilled remainder of latest order is left on the book
        assertThat(book.getTotalBuyQuantity()).isEqualTo(0);
        assertThat(book.getTotalSellQuantity()).isEqualTo(20);
        assertThat(book.getSellOrders()).containsExactly(
                LimitOrder.from("1003", OrderSide.SELL, 20, (short) 105, 3));
    }

    @Test
    public void sell_limit_order_can_match_multiple_buy_limit_orders_in_price_and_time_priority() {
        // given
        OrderBook book = makeBook();
        addPassiveOrder(book, buy(20, 101));
        addPassiveOrder(book, buy(30, 101));
        addPassiveOrder(book, buy(10, 105));
        addPassiveOrder(book, buy(10, 105));
        // when
        final List<Trade> trades = book.applyOrder(sell(50, 101));
        // then
        assertThat(trades).containsExactly(
                trade("1003", "1005", 105, 10),
                trade("1004", "1005", 105, 10),
                trade("1001", "1005", 101, 20),
                trade("1002", "1005", 101, 10)
        ).inOrder();
        // unfilled remainder of latest order is left on the book
        assertThat(book.getTotalBuyQuantity()).isEqualTo(20);
        assertThat(book.getTotalSellQuantity()).isEqualTo(0);
        assertThat(book.getBuyOrders()).containsExactly(
                LimitOrder.from("1002", OrderSide.BUY, 20, (short) 101, 2));
    }

    @Test
    public void sell_limit_order_can_match_multiple_buy_limit_orders_without_exceeding_price_limit() {
        // given
        OrderBook book = makeBook();
        addPassiveOrder(book, buy(20, 101));
        addPassiveOrder(book, buy(30, 101));
        addPassiveOrder(book, buy(10, 105));
        addPassiveOrder(book, buy(10, 105));
        // when
        final List<Trade> trades = book.applyOrder(sell(40, 102));
        // then
        assertThat(trades).containsExactly(
                trade("1003", "1005", 105, 10),
                trade("1004", "1005", 105, 10)
        ).inOrder();
        // unfilled remainder of the sell order is left on the book
        assertThat(book.getTotalBuyQuantity()).isEqualTo(50);
        assertThat(book.getTotalSellQuantity()).isEqualTo(20);
        assertThat(book.getSellOrders()).containsExactly(
                LimitOrder.from("1005", OrderSide.SELL, 20, (short) 102, 5));
    }

    /**
     * Scenario from the spec:
     * <p>
     * <pre>
     * BUY                SELL
     * Volume Price (p)   Price (p) Volume
     * 50,000       99    100       500
     * 25,500       98    100       10,000 (100,000) <- iceberg
     * </pre>
     * <p>
     * Iceberg SELL order with peak 10,000 and total 100,000 at price 100.
     * <p>
     * Assume now that an at best order to buy 16,000 shares is entered.
     * <p>
     * Specifically, the iceberg order has brought the following market efficiencies:
     * <ul>
     * <li>The originator of the iceberg order would have had 15,500 executed as opposed to just 10,000</li>
     * <li>The originator of the At Best order has traded all its volume at 100p at a consideration of £16,000
     * thanks to the iceberg order.</li>
     * </ul>
     */
    @Test
    public void when_book_has_iceberg_order_trading_does_not_pass_it_until_all_hidden_volume_is_exhausted() {
        // given
        OrderBook book = makeBook();
        addPassiveOrder(book, buy(50_000, 99));
        addPassiveOrder(book, buy(25_500, 98));
        addPassiveOrder(book, sell(500, 100));
        addPassiveOrder(book, sellIceberg(100_000, 10_000, 100));
        addPassiveOrder(book, sell(100, 103));
        addPassiveOrder(book, sell(20_000, 105));

        assertThat(book.getTotalBuyQuantity()).isEqualTo(75_500);
        assertThat(book.getTotalSellQuantity()).isEqualTo(120_600);
        assertThat(book.getTotalDisclosedBuyQuantity()).isEqualTo(75_500);
        assertThat(book.getTotalDisclosedSellQuantity()).isEqualTo(30_600);
        // when
        final List<Trade> trades = book.applyOrder(buy(16_000, 110));
        // then
        assertThat(trades).containsExactly(
                trade("1007", "1003", 100, 500),
                trade("1007", "1004", 100, 15_500)
        ).inOrder();
        assertThat(book.getTotalBuyQuantity()).isEqualTo(75_500);
        assertThat(book.getTotalSellQuantity()).isEqualTo(104_600);
        assertThat(book.getTotalDisclosedBuyQuantity()).isEqualTo(75_500);
        assertThat(book.getTotalDisclosedSellQuantity()).isEqualTo(24_600);
        assertThat(book.getSellOrders()).contains(
                IcebergOrder.from("1004", OrderSide.SELL, 84_500, 4_500, 10_000, (short) 100, 8)
        );
    }

    /**
     * 4.2.3.1 Aggressive iceberg order entry
     * <p>
     * When an iceberg enters an order book aggressively, it will participate with its full volume.
     * Any remaining volume of the order will then be shown to the market in peaks of a size specified by the participant.
     * <p>
     * For example, an iceberg order to buy 100,000 shares at a price of 100p
     * is entered at 8:25:00 into the above order book,
     * and the participant has elected to define the peak size as 10,000 shares.
     * The iceberg order will enter the order book aggressively,
     * immediately matching against the two sell orders at 100p.
     * These trades are effected, and the remaining iceberg size is 82,500 shares.
     * <p>
     * The first peak of 10,000 shares is then entered into the order book appearing as a conventional limit order.
     */
    @Test
    public void when_iceberg_order_aggresses_book_trades_are_made_and_iceberg_order_reenters_book() {
        // given
        OrderBook book = makeBook();
        final LimitOrder buyOrder1 = buy(50_000, 99);
        final LimitOrder buyOrder2 = buy(25_500, 98);
        addPassiveOrder(book, buyOrder1);
        addPassiveOrder(book, buyOrder2);
        final LimitOrder sellOrder1 = sell(20_000, 101);
        addPassiveOrder(book, sellOrder1);
        addPassiveOrder(book, sell(10_000, 100));
        addPassiveOrder(book, sell(7_500, 100));
        // when
        final List<Trade> trades = book.applyOrder(buyIceberg(100_000, 10_000, 100));
        // then
        assertThat(trades).containsExactly(
                trade("1006", "1004", 100, 10_000),
                trade("1006", "1005", 100, 7_500)
        ).inOrder();
        assertThat(book.getTotalBuyQuantity()).isEqualTo(158_000);
        assertThat(book.getTotalSellQuantity()).isEqualTo(20_000);
        assertThat(book.getTotalDisclosedBuyQuantity()).isEqualTo(85_500);
        assertThat(book.getTotalDisclosedSellQuantity()).isEqualTo(20_000);

        assertThat(book.getBuyOrders()).containsExactly(
                IcebergOrder.from("1006", OrderSide.BUY, 82_500, 10_000, (short) 100, 7),
                buyOrder1, buyOrder2
        ).inOrder();
        assertThat(book.getSellOrders()).containsExactly(sellOrder1);
    }

    /**
     * 4.2.3.2 Passive iceberg order execution
     * <p>
     * Suppose now that an order to sell 10,000 shares At Best is entered at 8:25:32 in our previous example.
     * This incoming order will fully execute the visible peak of the iceberg. Once matching has occurred,
     * the trading system will automatically refresh the peak in the order book, assigning it a new time stamp.
     * Total remaining iceberg volume is then 72,500 shares – 10,000 of which are visible in the order book as below.
     */
    @Test
    public void when_iceberg_order_is_passive_trades_are_made_and_iceberg_order_reenters_book() {
        // given
        OrderBook book = makeBook();
        final LimitOrder buyOrder1 = buy(50_000, 99);
        final LimitOrder buyOrder2 = buy(25_500, 98);
        addPassiveOrder(book, buyOrder1);
        addPassiveOrder(book, buyOrder2);
        final LimitOrder sellOrder1 = sell(20_000, 101);
        addPassiveOrder(book, sellOrder1);
        addPassiveOrder(book, sell(10_000, 100));
        addPassiveOrder(book, sell(7_500, 100));
        book.applyOrder(buyIceberg(100_000, 10_000, 100));

        assertThat(book.getTotalBuyQuantity()).isEqualTo(158_000);
        assertThat(book.getTotalSellQuantity()).isEqualTo(20_000);
        assertThat(book.getTotalDisclosedBuyQuantity()).isEqualTo(85_500);
        assertThat(book.getTotalDisclosedSellQuantity()).isEqualTo(20_000);

        // when an order to sell 10,000 shares At Best is entered
        final List<Trade> trades = book.applyOrder(sell(10_000, 99));

        // then
        // This incoming order will fully execute the visible peak of the iceberg.
        assertThat(trades).containsExactly(
                trade("1006", "1007", 100, 10_000)
        );
        // Once matching has occurred, the trading system will automatically refresh the peak in the order book,
        // assigning it a new time stamp.
        // Total remaining iceberg volume is then 72,500 shares – 10,000 of which are visible in the order book.
        assertThat(book.getBuyOrders()).containsExactly(
                IcebergOrder.from("1006", OrderSide.BUY, 72_500, 10_000, (short) 100, 9),
                buyOrder1, buyOrder2
        ).inOrder();
        assertThat(book.getSellOrders()).containsExactly(sellOrder1);

        assertThat(book.getTotalBuyQuantity()).isEqualTo(148_000);
        assertThat(book.getTotalSellQuantity()).isEqualTo(20_000);
        assertThat(book.getTotalDisclosedBuyQuantity()).isEqualTo(85_500);
        assertThat(book.getTotalDisclosedSellQuantity()).isEqualTo(20_000);
    }

    /**
     * 4.2.3.2 Passive iceberg order execution (continued part 1)
     * <p>
     * Multiple executions of an iceberg order on the order book will only generate a single trade message (5TG)
     * for the iceberg participant (ie when an incoming order executes against the peak of an iceberg order and
     * some or all of the hidden volume).
     * <p>
     * To show this, consider an order to sell 11,000 At Best which enters the above order book at 8:26:12.
     * Since trading cannot pass through the price limit of 100p whilst there
     * is still some volume of the iceberg to be satisfied, the incoming order will match against
     * the revealed peak of the iceberg order, and 1,000 shares of the hidden volume.
     * The trading system will disseminate these trades in a single message (5TG), listing the separate trade codes.
     * <p>
     * The total volume of our iceberg order is now 61,500 shares, and the trading system must update the order book
     * with the new peak of the iceberg.
     * <p>
     * In the above example, the new peak will be 9,000 shares (peak size minus the 1,000 shares already matched of this peak)
     * and the partially executed peak will retain price priority. It will only be refreshed when a subsequent execution
     * takes the remaining peak size.
     * <p>
     * The execution will refresh the peak and generate a new time priority.
     * The order book will therefore appear as below.
     */
    @Test
    public void when_iceberg_order_is_executed_multiple_times_only_one_trade_is_made() {
        // given
        OrderBook book = makeBook();
        final LimitOrder buyOrder1 = buy(50_000, 99);
        final LimitOrder buyOrder2 = buy(25_500, 98);
        addPassiveOrder(book, buyOrder1);
        addPassiveOrder(book, buyOrder2);
        final LimitOrder sellOrder1 = sell(20_000, 101);
        addPassiveOrder(book, sellOrder1);
        addPassiveOrder(book, sell(10_000, 100));
        addPassiveOrder(book, sell(7_500, 100));
        book.applyOrder(buyIceberg(100_000, 10_000, 100));
        book.applyOrder(sell(10_000, 99));

        assertThat(book.getTotalBuyQuantity()).isEqualTo(148_000);
        assertThat(book.getTotalSellQuantity()).isEqualTo(20_000);
        assertThat(book.getTotalDisclosedBuyQuantity()).isEqualTo(85_500);
        assertThat(book.getTotalDisclosedSellQuantity()).isEqualTo(20_000);

        // when an order to sell 11,000 At Best which enters the above order book at 8:26:12.
        final List<Trade> trades = book.applyOrder(sell(11_000, 100));

        // then
        // Since trading cannot pass through the price limit of 100p whilst there
        // is still some volume of the iceberg to be satisfied, the incoming order will match against
        // the revealed peak of the iceberg order, and 1,000 shares of the hidden volume.
        // The trading system will disseminate these trades in a single message.
        assertThat(trades).containsExactly(
                trade("1006", "1008", 100, 11_000)
        );
        // The total volume of our iceberg order is now 61,500 shares, and the trading system must update the order book
        // with the new peak of the iceberg.
        // In the above example, the new peak will be 9,000 shares
        // (peak size minus the 1,000 shares already matched of this peak)
        // and the partially executed peak will retain price priority.
        final ImmutableList<Order> buyOrders = book.getBuyOrders();
        assertThat(buyOrders.get(0).getId()).isEqualTo("1006");
        assertThat(buyOrders.get(0).getSide()).isEqualTo(OrderSide.BUY);
        assertThat(buyOrders.get(0).getQuantity()).isEqualTo(61_500);
        assertThat(buyOrders.get(0).getDisclosedQuantity()).isEqualTo(9_000);
        assertThat(buyOrders.get(0).getTimestamp()).isEqualTo(11);

        assertThat(buyOrders.subList(1, buyOrders.size())).containsExactly(buyOrder1, buyOrder2).inOrder();
        assertThat(book.getSellOrders()).containsExactly(sellOrder1);

        assertThat(book.getTotalBuyQuantity()).isEqualTo(137_000);
        assertThat(book.getTotalSellQuantity()).isEqualTo(20_000);
        assertThat(book.getTotalDisclosedBuyQuantity()).isEqualTo(84_500);
        assertThat(book.getTotalDisclosedSellQuantity()).isEqualTo(20_000);
    }

    /**
     * 4.2.3.2 Passive iceberg order execution (continued part 2)
     * <p>
     * Suppose that a second iceberg order is entered at 8:28:00 to buy 50,000 shares at a limit price of 100p,
     * with a revealed peak size of 20,000 shares.
     * To distinguish the iceberg orders, they have been labeled A and B.
     * The first peak of the new iceberg (B) will be entered into the order book, which will now appear as below.
     * <p>
     * When there are multiple icebergs at a single price level, then the new peaks of the iceberg orders retain time priority
     * amongst themselves. For example, if an order to sell 35,000 shares At Best is now entered at 8:30,
     * then the visible peaks of both icebergs will be completely filled,
     * and iceberg A will satisfy the remaining 6,000 shares of the incoming order.
     * <p>
     * The market will see two trade reports from this series of executions – one for 15,000 shares (against iceberg A),
     * and one for 20,000 shares (against iceberg B).
     * <p>
     * Once matching has been completed, new peaks of both icebergs are introduced to the order book,
     * with iceberg A retaining priority over iceberg B.
     */
    @Test
    public void when_multiple_iceberg_order_are_at_the_same_price_level_they_are_executed_in_time_priority() {
        // given
        OrderBook book = makeBook();
        final LimitOrder buyOrder1 = buy(50_000, 99);
        final LimitOrder buyOrder2 = buy(25_500, 98);
        addPassiveOrder(book, buyOrder1);
        addPassiveOrder(book, buyOrder2);
        final LimitOrder sellOrder1 = sell(20_000, 101);
        addPassiveOrder(book, sellOrder1);
        addPassiveOrder(book, sell(10_000, 100));
        addPassiveOrder(book, sell(7_500, 100));
        book.applyOrder(buyIceberg(100_000, 10_000, 100));
        book.applyOrder(sell(10_000, 99));
        book.applyOrder(sell(11_000, 99));

        // a second iceberg order is entered at 8:28:00 to buy 50,000 shares at a limit price of 100p
        // with a revealed peak size of 20,000 shares
        addPassiveOrder(book, buyIceberg(50_000, 20_000, 100));

        assertThat(book.getTotalBuyQuantity()).isEqualTo(187_000);
        assertThat(book.getTotalSellQuantity()).isEqualTo(20_000);
        assertThat(book.getTotalDisclosedBuyQuantity()).isEqualTo(104_500);
        assertThat(book.getTotalDisclosedSellQuantity()).isEqualTo(20_000);

        // when an order to sell 35,000 shares At Best is now entered at 8:30,
        final List<Trade> trades = book.applyOrder(sell(35_000, 100));

        // then
        // the visible peaks of both icebergs will be completely filled,
        // and iceberg A will satisfy the remaining 6,000 shares of the incoming order.
        // The market will see two trade reports from this series of executions –
        // one for 15,000 shares (against iceberg A),
        // and one for 20,000 shares (against iceberg B).
        assertThat(trades).containsExactly(
                trade("1006", "1010", 100, 15_000),
                trade("1009", "1010", 100, 20_000)
        );
        // Once matching has been completed, new peaks of both icebergs are introduced to the order book,
        // with iceberg A retaining priority over iceberg B.
        //     * Time Volume Price (p) Price (p) Volume Time
        //     * 8:30:00 8:30:00 8:20:25
        //     * 4,000A 20,000B 50,000
        //     * 100 100 99
        final ImmutableList<Order> buyOrders = book.getBuyOrders();
        assertThat(buyOrders.get(0).getId()).isEqualTo("1006");
        assertThat(buyOrders.get(0).getSide()).isEqualTo(OrderSide.BUY);
        assertThat(buyOrders.get(0).getQuantity()).isEqualTo(46_500);
        assertThat(buyOrders.get(0).getDisclosedQuantity()).isEqualTo(4_000);
        assertThat(buyOrders.get(0).getTimestamp()).isEqualTo(14);

        assertThat(buyOrders.get(1).getId()).isEqualTo("1009");
        assertThat(buyOrders.get(1).getSide()).isEqualTo(OrderSide.BUY);
        assertThat(buyOrders.get(1).getQuantity()).isEqualTo(30_000);
        assertThat(buyOrders.get(1).getDisclosedQuantity()).isEqualTo(20_000);
        assertThat(buyOrders.get(1).getTimestamp()).isEqualTo(15);

        assertThat(buyOrders.get(2)).isEqualTo(buyOrder1);
        assertThat(book.getSellOrders()).containsExactly(sellOrder1);

        assertThat(book.getTotalBuyQuantity()).isEqualTo(152_000);
        assertThat(book.getTotalSellQuantity()).isEqualTo(20_000);
        assertThat(book.getTotalDisclosedBuyQuantity()).isEqualTo(99_500);
        assertThat(book.getTotalDisclosedSellQuantity()).isEqualTo(20_000);
    }
}
