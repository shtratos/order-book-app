.PHONY: all
all:  build run-simple-example run-iceberg-example

.PHONY: build
build:
	./gradlew clean build --stacktrace

.PHONY: package
package:
	rm -f ~/orderbook.zip
	zip -r ~/orderbook.zip * --exclude build/\* .gradle .idea

.PHONY: run-simple-example
run-simple-example:
	./gradlew --quiet run < ./src/test/resources/examples/limit_orders_input.txt > /tmp/simple-test.txt
	diff /tmp/simple-test.txt ./src/test/resources/examples/limit_orders_output.txt
	echo "== INPUT =="
	cat ./src/test/resources/examples/limit_orders_input.txt
	echo "== OUTPUT =="
	cat /tmp/simple-test.txt

.PHONY: run-iceberg-example
run-iceberg-example:
	./gradlew --quiet run < ./src/test/resources/examples/iceberg_orders_input.txt > /tmp/iceberg-test.txt
	diff /tmp/iceberg-test.txt ./src/test/resources/examples/iceberg_orders_output.txt
	echo "== INPUT =="
	cat ./src/test/resources/examples/iceberg_orders_input.txt
	echo "== OUTPUT =="
	cat /tmp/iceberg-test.txt

