plugins {
    java
    application
    id("net.ltgt.errorprone") version "1.1.1"
}

group = "com.github.shtratos.orderbook"
version = "1.0-SNAPSHOT"

application {
    mainClassName = "com.github.shtratos.orderbook.Main"

    // allow applications launched via Gradle to use stdin
    // see https://stackoverflow.com/a/46662535/7417402
    val run by tasks.getting(JavaExec::class)
    run.standardInput = System.`in`
}


repositories {
    mavenCentral()
}

dependencies {
    // configure static analysis: https://errorprone.info/
    errorprone("com.google.errorprone:error_prone_core:2.3.4")
    errorproneJavac("com.google.errorprone:javac:9+181-r4173-1")

    implementation("com.google.guava:guava:28.2-jre")
    implementation("de.vandermeer:asciitable:0.3.2")

    testCompile("junit:junit:4.12")
    testImplementation("com.google.truth:truth:1.0.1")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}
