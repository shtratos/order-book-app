# Orderbook exercise

This project implements a simple order book that supports limit and iceberg orders
as described in SETSmm LSE specification. 

## Project Structure
 
  - `specs` - specification documents 
  - `src/main/java` - implementation source code  
  - `src/test/java` - tests source code  
  - `src/test/resources/examples` - some examples of valid input and expected output
  - `build.gradle.kts` - Gradle build script written in Kotlin
  - `gradlew`, `gradlew.bat` - Gradle launch scripts for Linux/macOS and Windows
  - `Makefile` - helper tasks for easily building and launching the application 

## Running the application 

### Prerequisites

 - Linux or macOS platform. 
   The app itself should be able to run on Windows, 
   but some minor tweaks for launch commands will be required.  
 - JDK 8+ installed and accessible on your path
 - Internet connection
 - `make` tool (not required, but it simplifies launch commands)

### Building

```bash
    make build
```

Alternatively:

```bash
    ./gradlew build
```

This will download Gradle binary, build and test the project.   

### Running examples

There are 2 examples included in `src/test/resources/examples` directory:

- `limit_orders_input.txt` - basic example with one buy and one sell order

    To run it:
    
    ```bash
        make run-simple-example
    ```
  
    It will run the app, send the example file as input 
    and diff with expected output in `limit_orders_output.txt`
    
    Alternatively, you can run it directly:
    
    ```bash
    ./gradlew --quiet run < ./src/test/resources/examples/limit_orders_input.txt   
    ```
    
- `iceberg_orders_input.txt` - a scenario with Iceberg Orders from section 4.2.3 of SETSmm specification 

    To run it:
    
    ```bash
        make run-iceberg-example
    ```
  
    It will run the app, send the example file as input 
    and diff with expected output in `iceberg_orders_output.txt`
    
    Alternatively, you can run it directly:
    
    ```bash
    ./gradlew --quiet run < ./src/test/resources/examples/iceberg_orders_input.txt   
    ```

## Notes on implementation

- Took me a while to discover how orders with overlapping prices should work.
  I've found the answer in LSE Trading System guide.
- I did not go overboard with javadoc, testing absolutely everything, 
  writing useful assertion messages and putting nullability checks and annotations.
  Still the line test coverage is close to 100% as my IDE shows me.
  I've also included compile time static analysis via [Google Errorprone](https://errorprone.info)   
- I did not bothered with thread-safety since the application operation does not require it.
- I tried to focus on making the code readable and easy to maintain. 
  For that reason I did not care much about performance.
- I did not generalise things too much - having less interfaces makes it easier to read. 
  Extra flexibility can always be added via refactoring and we have tests to prevent regressions.
- I did not use Git to keep track of all the changes since I do not plan to put this on Github.
  Also I decided that carefully crafting commits and commit messages in this particular case
  is less important than finally delivering the solution :)

Overall, this was a nice self-contained problem.
It made me learn more about how stock exchanges work and 
appreciate the complexity of order matching systems.

If you have any questions, don't hesitate to reach me at [dmitry.stratiychuk@gmail.com](mailto:dmitry.stratiychuk@gmail.com) 

Thank you! 
